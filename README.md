# 360� Virtual Tour of Carlos Place

https://5carlosplace.glitch.me/

![screenshot](https://bitbucket.org/sarahga/hackathon-team-1/raw/8a305a8656f6239288bb04affdd83632b73498d4/screenshot.png)

Built with [A-Frame](https://aframe.io), a web framework for building virtual reality experiences. Make WebVR with HTML and Entity-Component. Works on Vive, Rift, desktop, mobile platforms.

Click and drag on desktop. Open it on a smartphone and use the device motion sensors. Or [plug in a VR headset](https://webvr.rocks)!