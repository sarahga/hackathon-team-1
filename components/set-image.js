/* global AFRAME */

/**
 * Component that listens to an event, fades out an entity, swaps the texture, and fades it
 * back in.
 */

AFRAME.registerComponent('set-image', {
  schema: {
    on: {type: 'string'},
    target: {type: 'selector'},
    src: {type: 'string'},
    dur: {type: 'number', default: 300}
  },

  init: function () {
    var data = this.data;
    var el = this.el;

    this.setupFadeAnimation();

    el.addEventListener(data.on, function () {
      // Fade out image.
      data.target.emit('set-image-fade');
      // Wait for fade to complete.
      setTimeout(function () {
        var camera = document.getElementById('camera');
        // Set image.
        if (data.src === '#Outside') {
          document.getElementById('outside').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('doorstep').setAttribute('position', {x: -4, y: 1, z: 0.4});
          document.getElementById('doorstep').setAttribute('rotation', {x: 0, y: 90, z: 0});
          document.getElementById('staircase').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('garden').setAttribute('position', {x: 51, y: 51, z: 51});
        } else if (data.src === '#Doorstep') {
          camera.setAttribute('rotation', {x: 0, y: 90, z: 0});
          document.getElementById('doorstep').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('outside').setAttribute('position', {x: 3, y: 0, z: 0.1});
          document.getElementById('outside').setAttribute('rotation', {x: 0, y: -90, z: 0});
          document.getElementById('staircase').setAttribute('position', {x: -4, y: 1.4, z: 0.7});
          document.getElementById('staircase').setAttribute('rotation', {x: 0, y: 90, z: 0});
          document.getElementById('garden').setAttribute('position', {x: -6, y: 0, z: -2});
          document.getElementById('garden').setAttribute('rotation', {x: 0, y: 90, z: 0});
          document.getElementById('gardenpodcast').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('sculpture').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('womens').setAttribute('position', {x: 51, y: 51, z: 51});
        } else if (data.src === '#Staircase') {
          camera.setAttribute('rotation', {x: -0.8, y: 42, z: -5});
          document.getElementById('outside').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('staircase').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('garden').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('doorstep').setAttribute('position', {x: 0.7, y: -4, z: 4});
          document.getElementById('doorstep').setAttribute('rotation', {x: -35, y: 180, z: 5});
          document.getElementById('sculpture').setAttribute('position', {x: 6, y: -3.5, z: 5});
          document.getElementById('sculpture').setAttribute('rotation', {x: 0, y: -90, z: 0});
          document.getElementById('womens').setAttribute('position', {x: -1, y: 0, z: -2});
          document.getElementById('womens').setAttribute('rotation', {x: 0, y: 50, z: 0});
          document.getElementById('shoe').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('bag').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('mens').setAttribute('position', {x: 51, y: 51, z: 51});
        } else if (data.src === '#Garden') {
          camera.setAttribute('rotation', {x: -0.11, y: 51, z: 0});
          document.getElementById('outside').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('staircase').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('garden').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('doorstep').setAttribute('position', {x: -6, y: -0.5, z: 0.5});
          document.getElementById('doorstep').setAttribute('rotation', {x: 0, y: 90, z: 0});
          document.getElementById('gardenpodcast').setAttribute('position', {x: -1, y: -1.5, z: -4});
          document.getElementById('gardenpodcast').setAttribute('rotation', {x: 0, y: 35, z: 0});
        } else if (data.src === '#Womens') {
          camera.setAttribute('rotation', {x: -5, y: 443, z: -5});
          document.getElementById('doorstep').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('sculpture').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('womens').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('staircase').setAttribute('position', {x: 1.3, y: 0, z: -3});
          document.getElementById('staircase').setAttribute('rotation', {x: 0, y: 0, z: 0});
          document.getElementById('mens').setAttribute('position', {x: 3, y: 0, z: -0.6});
          document.getElementById('mens').setAttribute('rotation', {x: 0, y: -90, z: 0});
          document.getElementById('bag').setAttribute('position', {x: -7, y: -3, z: 4});
          document.getElementById('bag').setAttribute('rotation', {x: 0, y: 110, z: 0});
          document.getElementById('shoe').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('baggif').setAttribute("visible",false);
          document.getElementById('womenscross').setAttribute('position', {x: 51, y: 51, z: 51});
        } else if (data.src === '#Mens') {
          camera.setAttribute('rotation', {x: -2.25, y: 267, z: -5});
          document.getElementById('bag').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('mens').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('womens').setAttribute('position', {x: -3, y: 0, z: -0.2});
          document.getElementById('womens').setAttribute('rotation', {x: 0, y: 90, z: 0});
          document.getElementById('staircase').setAttribute('position', {x: -2.5, y: 0, z: -3});
          document.getElementById('staircase').setAttribute('rotation', {x: 0, y: 60, z: 5});
          document.getElementById('shoe').setAttribute('position', {x: 5, y: -3.1, z: 4});
          document.getElementById('shoe').setAttribute('rotation', {x: 0, y: -110, z: 0});
        } else if (data.src === '#Black') {
          document.getElementById('staircase').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('mens').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('bag').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('baggif').setAttribute("visible",true);
          document.getElementById('womenscross').setAttribute('position', {x: -3, y: 2, z: -2.8});
          document.getElementById('womenscross').setAttribute('rotation', {x: 0, y: 90, z: 0});
        }else if (data.src === '#Team') {
          document.getElementById('doorstep').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('sculpture').setAttribute('position', {x: 51, y: 51, z: 51});
          document.getElementById('womens').setAttribute('position', {x: 51, y: 51, z: 51});
        }
        // else if (data.src === '#Bag') {
        //   document.getElementById('myScene').exitVR();
        //   window.open('https://cdn.glitch.com/27eabdc9-f788-4090-93df-287042856755%2FUntitled2.gif?1537491895137');
        //   return;
        // }
        data.target.setAttribute('material', 'src', data.src);
      }, data.dur);
    });
  },

  /**
   * Setup fade-in + fade-out.
   */
  setupFadeAnimation: function () {
    var data = this.data;
    var targetEl = this.data.target;

    // Only set up once.
    if (targetEl.dataset.setImageFadeSetup) { return; }
    targetEl.dataset.setImageFadeSetup = true;

    // Create animation.
    targetEl.setAttribute('animation__fade', {
      property: 'material.color',
      startEvents: 'set-image-fade',
      dir: 'alternate',
      dur: data.dur,
      from: '#FFF',
      to: '#000'
    });
  }
});
